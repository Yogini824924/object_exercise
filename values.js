//function values(obj) {
  // Return all of the values of the object's own properties.
  // Ignore functions
  // http://underscorejs.org/#values
//}
/* const keys = require("./keys.js");
  function values(obj) {
	if(!(typeof obj === "object")) {
		return [];
	}
	let value = [], key = keys(obj);
	for(let i = 0; i < key.length; i++) {
		value.push(obj[key[i]]);
	}
	return value;
}
*/
/*
const keys = require("./keys.js");
function values(obj) {
	if(!(typeof obj === "object")) {
		return [];
	}
	let key = keys(obj), kl = key.length, value = Array(kl);
	for( let i = 0; i < kl; i++) {
		value[i] = obj[key[i]];
	}
	return value;
}
*/

function values(obj) {
	if(!(typeof obj === "object")) {
		return [];
	}
	let value = [];
	for( let key in obj) {
		value.push(obj[key]);
	}
	return value;
}

module.exports = values;
