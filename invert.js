//function invert(obj) {
  // Returns a copy of the object where the keys have become the values and the values the keys.
  // Assume that all of the object's values will be unique and string serializable.
  // http://underscorejs.org/#invert
//}


function invert(obj) {
	if(!(typeof obj === "object")) {
		return [];
	}
	let invObj = {};
	for(let key in obj) {
		invObj[obj[key]] = key;
	}
	return invObj;
}

/*
const keys = require("./keys.js");
const values = require("./values.js");

function invert(obj) {
	if(!(typeof obj === "object")) {
		return [];
	}
	let invObj = {}, key = keys(obj), value = values(obj), len = key.length;
	for(let i = 0; i < len; i++) {
		invObj[value[i]] = key[i];
	}
	return invObj;
}
*/
/*
const keys = require("./keys.js");

function invert(obj) {
	if(!(typeof obj === "object")) {
		return [];
	}
	let invObj = {}, key = keys(obj), len = key.length;
	for(let i = 0; i < len; i++) {
		invObj[obj[key[i]]] = key[i];
	}
	return invObj;
}
*/

module.exports = invert;
