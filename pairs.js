//function pairs(obj) {
  // Convert an object into a list of [key, value] pairs.
  // http://underscorejs.org/#pairs
//}
/*
const keys = require("./keys.js");
const values = require("./values.js");
function pairs(obj) {
	if(!(typeof obj === "object")) {
		return [];
	}
	let key = keys(obj), value = values(obj), len = key.length, pair = Array(len);
	for(let i = 0; i < len; i++) {
		pair[i] = [ key[i], value[i] ];
	}
	return pair;
}
*/
/*
const keys = require("./keys.js");
function pairs(obj) {
	if(!(typeof obj === "object")) {
		return [];
	}
	let key = keys(obj), len = key.length, pair = Array(len);
	for(let i = 0; i < len; i++) {
		pair[i] = [ key[i], obj[key[i]] ];
	}
	return pair;
}
*/
function pairs(obj) {
	if(!(typeof obj === "object")) {
		return [];
	}
	let pair = [];
	for( let key in obj) {
		let temp = [];
		temp.push(key);
		temp.push(obj[key]);
		pair.push(temp);
	}
	return pair;
}

module.exports = pairs;
