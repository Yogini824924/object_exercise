//function defaults(obj, defaultProps) {
  // Fill in undefined properties that match properties on the `defaultProps` parameter object.
  // Return `obj`.
  // http://underscorejs.org/#defaults
//}


function defaults(obj, defaultProps) {
	if(typeof obj === "object" && !(typeof defaultProps === "object")) {
		return obj;
	}
	else if(!(typeof obj === "object" )) {
		return [];
	}
	for( let key in defaultProps) {
		if(!(obj.hasOwnProperty(key))) {
			obj[key] = defaultProps[key];
		}
	}
	return obj;
}


module.exports = defaults;
